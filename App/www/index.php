<?php
/**
 *
 * Description of index.php
 *
 * Example of application index using the Framework
 *
 * @author Pawel Maryanov pawelmareyn@gmail.com
 * @created 03/05/16 16:58
 *
*/

require_once __DIR__ . '/../../Cetus2/Framework/Core/Launcher.php';

$launcherObj = new \Framework\Core\Launcher();
$launcherObj->run();