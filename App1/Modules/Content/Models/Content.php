<?php
/**
 *
 * Description of Content
 *
 * @author Pawel Maryanov pawelmareyn@gmail.com
 * @created 31/05/16 17:37
 *
*/

namespace App1\Modules\Content\Models;


class Content extends \Framework\Lib\Modules\Content\Models\Content
{
    public function __construct($module)
    {
        parent::__construct($module);

        $feedback = $this->getModel('Feedback');
        $feedback->test();

        echo 'Custom module Content loaded!', '<br>';
    }
}