<?php
/**
 *
 * Description of ContentController.php
 * 
 * @author Pawel Maryanov pawelmareyn@gmail.com
 * @created 31/05/16 18:39
 *
*/

namespace App1\Modules\Content\Controllers;

use \Framework\Lib;

class ContentController extends Lib\Modules\Content\Controllers\ContentController
{
    public function defaultAction()
    {

    }
}