<?php
/**
 *
 * Description of ContentModule
 *
 * @author Pawel Maryanov pawelmareyn@gmail.com
 * @created 01/06/16 19:44
 *
*/

namespace App1\Modules\Content;

use \Framework\Core;

use \Framework\Lib;

class ContentModule extends \Framework\Lib\Modules\Content\ContentModule
{
    public function __construct()
    {
        parent::__construct(__NAMESPACE__);

        Lib\Logger::debug(__METHOD__ . ' ' . ': ' . print_r(__NAMESPACE__, true));

        /*$feedback = $this->getModel('Feedback');

        $feedback->test();*/

        /*
        $this->registerModel('Feedback');
        $this->registerModel('Content');

        $this->registerForeignModel('Users', 'Login');

        Lib\Logger::debug(__METHOD__ . ' ' . 'ContentModule __construct Done: ');*/
    }
}